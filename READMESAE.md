# Installation de Xubuntu en dual boot

## Objectif attendu
Dans cette rédaction, je vais vous expliquer étapes par étapes comment
j'ai procédé à l'installation de **__Xubuntu__** sur mon ordinateur portable et comment vous allez également pouvoir le faire.

# Explications
Tout d'abord, il y a plusieurs manières d'installer Xubuntu sur son
ordinateur portable, même si il y a déjà un autre système d'exploitation
comme par exemple Windows. On peut procéder à une installation complète
de Xubuntu en formatant son ordinateur mais on peut également procéder à
un **__dual boot__**, ce qui veut dire que l'on aura 2 systèmes d'exploitation
sur son ordinateur. Cela permet de réduire les dépenses en matériel, en
évitant d'acquérir un autre ordinateur pour chaque système.

# Ce qui est nécessaire à l'installation :
Ensuite, passons à l'installation de Xubuntu. Pour commencer, il vous
faudra : 
- Une clé USB **__vierge__**  
- **__Rufus__**, qui est un logiciel libre, qui permet de créer des supports bootable sur un périphérique externe comme une clé USB par exemple
- Créer une **__partition__** sur laquelle nous allons installer notre OS 
- Un ordinateur

# Première étape :
## Création de la partition

Pour commencer, il faut **__créer une partition__** sur votre disque afin de
pouvoir y installer Xubuntu. Dans votre barre de recherche Windows,
tapez `«disque»` et cela va normalement vous afficher `«Créer et formater des partitions de disque dur»`
. Cliquez dessus, et cela vous ouvre
ensuite une fenêtre où vous voyez votre/vos disque(s). Choisissez celui
que vous voulez et faites un clic droit et faites réduire le volume.
Puis, dans quantité d'espace à réduire, vous allez mettre **__40 000__**, afin
de créer un espace de 40Go sur votre disque. Puis faites réduire.



# Seconde étape :
## Le système d'exploitation

Par la suite, il faut télécharger **__l'image ISO__** de Xubuntu en se rendant
directement sur le site de [Xubuntu](http://ftp.free.fr/mirrors/ftp.xubuntu.com/releases/22.04/release/xubuntu-22.04.1-desktop-amd64.iso). Il faut choisir la bonne version de Xubuntu afin de ne pas avoir d'erreur lors de l'installation. Une fois
que cela est fait, il nous faut installer [Rufus](http://rufus.ie/fr/), afin de boot notre clé
USB. C'est très simple, il suffit de se diriger dans l'espace
téléchargement et d'installer la version de Rufus 3.20.

Une fois que l'on a installer Rufus et que l'on a une clé vierge avec
l'image ISO sur celle-ci, branché à notre ordinateur, nous pouvons
exécuter Rufus. Une fois exécuter, nous avons une petite fenêtre qui
s'ouvre avec plein d'informations à compléter :

Dans l'onglet `«Options de périphérique»`:

-   Dans périphérique, nous allons choisir notre clé USB mais
    normalement, si elle est déjà branché à votre ordinateur, elle est
    censé être automatiquement reconnue.
-   En type de démarrage, il nous faut cliquer sur sélection et aller
    chercher le fichier **__ISO__** que nous avons téléchargé auparavant.
-   Nous laissons **__MBR__** en schéma de partition par défaut
-   En système de destination, choisir **__BIOS__** ou **__UEFI__**

Dans l'onglet `«Options de formatage»` :

-   Nom de volume correspond au nouveau nom de votre clé après le boot (
    ne le changez pas )
-   Choisissez **__FAT32__** par défaut en système de fichiers
-   En taille d'unité d'allocation, choisissez **__4096__** octets par défaut
    Après ça, on peut enfin cliquer sur `« Démarrer »` pour boot votre clé
    USB.

Après cela, c'est bon, vous avez enfin réussi a boot votre clé USB !
Mais pas de précipitation, ce n'est pas encore fini.

Il nous faut ensuite installer notre nouveau système d'exploitation.
Nous allons donc commencer par redémarrer l'ordinateur.

# Vous y êtes presque !

## Dernière ligne droite !

L'objectif va être d'accéder au **__BIOS__** , qui est un composant essentiel
d'un ordinateur. Afin d'accéder au BIOS, il nous faut lors du
redémarrage, appuyez sur un touche du clavier (elle varie selon
l'ordinateur, souvent en fonction de la marque) lorsqu'on voit
l'utilitaire.

Par exemple, sur un ordinateur de la marque ASUS, lors du redémarrage,
vous allez appuyez sur la touche F2 de votre clavier lorsque vous voyez
le logo ASUS. Une fois que vous avez trouvé la touche correspondante et
que vous êtes dans le BIOS, vous aurez besoin d'aller dans les `options de boot`
(boot options en anglais) et changer la priorité du `«First boot»`
et mettre notre clé USB à la place afin que la machine redémarre sur
notre clé USB boot.

Puis, faites **__« save and exit »__**. Votre pc va redémarrer sur la clé et va
proposé de démarrer sur plusieurs choses, dont Windows et Xubuntu.
Sélectionner Xubuntu et ensuite patientez jusqu'à la page
d'installation. Une fois sur la page d'installation, sélectionner la
langue (français de préférence) mais vous pouvez choisir la langue que
vous voulez. Ensuite, il va nous proposer plusieurs types
d'installations. Puisque nous voulons procéder à un dual boot, nous
allons donc sélectionner **__« Installer Xubuntu à côté de Windows Boot Manager »__**

Choisissez ensuite votre localisation fuseau horaire et définissez un
nom et un mot de passe afin de vous connecter. Puis, pour finir, cliquer
sur « Continuer » afin de terminer l'installation. Et voilà, vous êtes
enfin sur votre nouveau système d'exploitation, cela veut dire que vous
avez réussit ! Bravo !

Afin de bien être sûr que cela marche parfaitement, vous pouvez
redémarrer votre machine afin de voir si lors du redémarrage, il vous
propose Xubuntu ou Windows Boot Manager, puis redémarrer sous les deux
systèmes afin de voir si tout marche bien !
